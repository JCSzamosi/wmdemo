#! /bin/bash

# To use this script call it with the file basename as a command-line argument.

WW_DIR=/home/jcszamosi/Tools/WorkingWiki
bnm=`basename $1 .md`

# Preprocessing step
php $WW_DIR/wmd/wmd.php --pre --default-project-name=$bnm --cache-dir=./wmd_files --process-inline-math < $bnm.md > $bnm.interm.md

markdown $bnm.interm.md > $bnm.interm.html

#Postprocessing step
here=`pwd`
base=file://$here/wmd_files
php $WW_DIR/wmd/wmd.php --post --default-project-name=$bnm --cache-dir=./wmd_files --project-file-base-url=file://$base < $bnm.interm.html > $bnm.html


