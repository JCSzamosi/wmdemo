## makefile for WMDemo project
## this is suitable for use with other WMD projects
## change WW_DIR as needed
## change default_target if desired
## update the lines at the bottom that define project names for
## the different .md pages

WW_DIR=/home/jcszamosi/Tools/WorkingWiki

default_target: WMDemo.html

# Preprocessing step: encode the WMD tags to protect them from the
# markdown interpreter
INTERM := wmd_files/interm
$(INTERM)/%.interm.md : %.md $(INTERM) wmd_files/.workingwiki Makefile
	php $(WW_DIR)/wmd/wmd.php --pre --default-project-name=$(PROJECT)  --cache-dir=./wmd_files --process-inline-math < $< > $@

# Processing the markdown
$(INTERM)/%.interm.html : $(INTERM)/%.interm.md
	markdown $< > $@

# Postprocessing step: restore and execute the WMD directives
BASEURL = file://$(dir $(realpath $@))/wmd_files
%.html : $(INTERM)/%.interm.html
	php $(WW_DIR)/wmd/wmd.php --post --default-project-name=$(PROJECT) --cache-dir=./wmd_files --project-file-base-url=$(BASEURL) < $< > $@

# define what WMD project name goes with each page
WMDemo.html $(INTERM)/WMDemo.md : PROJECT = WMDemo

# bookkeeping stuff to make sure directories exist when needed
wmd_files/.workingwiki $(INTERM) :
	mkdir -p $@
